use std::{
    fmt::{Debug, Display},
    ops::{Add, AddAssign, BitAnd, Not, Sub, SubAssign},
};

use crate::RuntimeError;

#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct Integer<const BITS: u8>(u16);

impl<const BITS: u8> Integer<BITS> {
    const NUM_UNUSED_BITS: u8 = u16::BITS as u8 - BITS;
    const MAX_AS_STORED: u16 = u16::MAX << Self::NUM_UNUSED_BITS;

    pub(crate) const MAX: u16 = Self::MAX_AS_STORED >> Self::NUM_UNUSED_BITS;
    pub(crate) const SIGNED_MIN: i16 = (u16::MAX << (BITS - 1)) as i16;
    pub(crate) const SIGNED_MAX: i16 = (Self::MAX >> 1) as i16; // Shift out of sign bit
}

impl Integer<16> {
    pub const fn new(integer: u16) -> Self {
        Self(integer)
    }
}

impl Integer<8> {
    pub const fn new(integer: u8) -> Self {
        Self((integer as u16) << 8)
    }
}

impl<const BITS: u8> Debug for Integer<BITS> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mask = 0b1111;
        write!(
            f,
            "Integer<{}>(0b{:0>4b}_{:0>4b}_{:0>4b}_{:0>4b})",
            BITS,
            self.0 >> 12 & mask,
            self.0 >> 8 & mask,
            self.0 >> 4 & mask,
            self.0 & mask,
        )
    }
}

impl<const BITS: u8> Display for Integer<BITS> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Integer(0b")?;

        let value = u16::from(*self);
        for i in (0..BITS).rev() {
            write!(f, "{}", (value >> i) & 1)?;

            if i > 0 && i % 4 == 0 {
                write!(f, "_")?;
            }
        }

        write!(f, ")")
    }
}

impl<const BITS: u8> Default for Integer<BITS> {
    fn default() -> Self {
        Self(u16::default())
    }
}

// TODO: restrict BITS to not be 16 so From can be implemented for Integer<16>
// Needs language support for where clauses for const generics.
impl<const BITS: u8> TryFrom<u16> for Integer<BITS> {
    type Error = RuntimeError;

    fn try_from(value: u16) -> Result<Self, RuntimeError> {
        let value = value.to_le();

        if value > Self::MAX {
            Err(RuntimeError::ConversionOverflow)
        } else {
            Ok(Self(value << Self::NUM_UNUSED_BITS))
        }
    }
}

impl<const BITS: u8> From<Integer<BITS>> for u16 {
    fn from(value: Integer<BITS>) -> Self {
        value.0 >> Integer::<BITS>::NUM_UNUSED_BITS
    }
}

impl<const BITS: u8> TryFrom<i16> for Integer<BITS> {
    type Error = RuntimeError;

    fn try_from(value: i16) -> Result<Self, RuntimeError> {
        let value = value.to_le();

        if value < Self::SIGNED_MIN || value > Self::SIGNED_MAX {
            Err(RuntimeError::ConversionOverflow)
        } else {
            Ok(Self((value as u16) << Self::NUM_UNUSED_BITS))
        }
    }
}

impl<const BITS: u8> From<Integer<BITS>> for i16 {
    fn from(value: Integer<BITS>) -> Self {
        value.0 as i16 >> Integer::<BITS>::NUM_UNUSED_BITS
    }
}

impl<const BITS: u8> Add for Integer<BITS> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let lhs = self.0 as i16;
        let rhs = rhs.0 as i16;

        let (sum, _) = lhs.overflowing_add(rhs);

        Self(sum as u16)
    }
}

impl<const BITS: u8> AddAssign for Integer<BITS> {
    fn add_assign(&mut self, rhs: Self) {
        let lhs = self.0 as i16;
        let rhs = rhs.0 as i16;

        let (sum, _) = lhs.overflowing_add(rhs);

        self.0 = sum as u16;
    }
}

impl<const BITS: u8> Sub for Integer<BITS> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        let lhs = self.0 as i16;
        let rhs = rhs.0 as i16;

        let (sum, _) = lhs.overflowing_sub(rhs);

        Self(sum as u16)
    }
}

impl<const BITS: u8> SubAssign for Integer<BITS> {
    fn sub_assign(&mut self, rhs: Self) {
        let lhs = self.0 as i16;
        let rhs = rhs.0 as i16;

        let (sum, _) = lhs.overflowing_sub(rhs);

        self.0 = sum as u16;
    }
}

impl<const BITS: u8> BitAnd for Integer<BITS> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        Self(self.0 & rhs.0)
    }
}

impl<const BITS: u8> Not for Integer<BITS> {
    type Output = Self;

    fn not(self) -> Self::Output {
        Self(!self.0 & Self::MAX_AS_STORED)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use test_case::test_case;

    #[test]
    fn debug() {
        assert_eq!(
            "Integer<15>(0b1010_0110_1100_0010)".to_string(),
            format!("{:?}", Integer::<15>(0b1010_0110_1100_0010))
        );
    }

    #[test]
    fn display_4_bits() {
        assert_eq!(
            "Integer(0b1100)".to_string(),
            format!("{}", Integer::<4>::try_from(0b1100_u16).unwrap())
        );
    }

    #[test]
    fn display_7_bits() {
        assert_eq!(
            "Integer(0b010_1100)".to_string(),
            format!("{}", Integer::<7>::try_from(0b010_1100_u16).unwrap())
        );
    }

    #[test]
    fn display_16_bits() {
        assert_eq!(
            "Integer(0b1010_0110_1100_0011)".to_string(),
            format!(
                "{}",
                Integer::<16>::try_from(0b1010_0110_1100_0011_u16).unwrap()
            )
        );
    }

    #[test]
    fn default() {
        assert_eq!(Integer(0), Integer::<4>::default());
    }

    #[test_case(Integer::<4>::MAX => 0b0000_0000_0000_1111 ; "max")]
    #[test_case(Integer::<4>::MAX_AS_STORED => 0b1111_0000_0000_0000 ; "max as stored")]
    fn unsigned_bounds(bound: u16) -> u16 {
        bound
    }

    #[test_case(Integer::<4>::SIGNED_MIN => -8 ; "signed min")]
    #[test_case(Integer::<4>::SIGNED_MAX => 7 ; "signed max")]
    fn signed_bounds(bound: i16) -> i16 {
        bound
    }

    #[test]
    #[should_panic]
    fn convert_unsigned_overflow() {
        Integer::<4>::try_from(42_u16).unwrap();
    }

    #[test]
    #[should_panic]
    fn convert_signed_overflow() {
        Integer::<4>::try_from(42_i16).unwrap();
    }

    #[test]
    #[should_panic]
    fn convert_signed_underflow() {
        Integer::<4>::try_from(-10_i16).unwrap();
    }

    #[test_case(0, 0)]
    #[test_case(15, 0b1111_0000_0000_0000)]
    fn convert_unsigned(start: u16, raw: u16) {
        let value: Integer<4> = Integer::try_from(start).unwrap();
        assert_eq!(Integer::<4>(raw), value, "incorrect internal data");
        assert_eq!(start, u16::from(value), "bad conversion to u16");
    }

    #[test_case(-1, 0b1111_0000_0000_0000)]
    #[test_case(-2, 0b1110_0000_0000_0000)]
    #[test_case(0, 0)]
    #[test_case(-8, 0b1000_0000_0000_0000)]
    #[test_case(7, 0b0111_0000_0000_0000)]
    fn convert_signed(start: i16, raw: u16) {
        let value: Integer<4> = Integer::try_from(start).unwrap();
        assert_eq!(Integer::<4>(raw), value, "incorrect internal data");
        assert_eq!(start, i16::from(value), "bad conversion to i16");
    }

    #[test_case(0b0010, 0b0010, 0b0100 ; "both positive")]
    #[test_case(0b1110, 0b1110, 0b1100 ; "both negative")]
    #[test_case(0b0010, 0b1111, 0b0001 ; "positive sum")]
    #[test_case(0b1110, 0b0001, 0b1111 ; "negative sum")]
    #[test_case(0b1111, 0b0010, 0b0001 ; "overflow")]
    #[test_case(0b1111, 0b0001, 0b0000 ; "overflow exactly")]
    #[test_case(0b1000, 0b1110, 0b0110 ; "underflow")]
    #[test_case(0b1000, 0b1111, 0b0111 ; "underflow exactly")]
    fn add(lhs: u16, rhs: u16, expected: u16) {
        let lhs = Integer::<4>::try_from(lhs).unwrap();
        let rhs = Integer::<4>::try_from(rhs).unwrap();
        let expected = Integer::<4>::try_from(expected).unwrap();

        assert_eq!(expected, lhs + rhs, "immutable");

        let mut sum = lhs;
        sum += rhs;

        assert_eq!(expected, sum, "add assign");
    }

    #[test_case(0b0100, 0b0010, 0b0010 ; "both positive")]
    #[test_case(0b1110, 0b1111, 0b1111 ; "both negative")]
    #[test_case(0b0010, 0b1111, 0b0011 ; "pos minus neg")]
    #[test_case(0b1111, 0b0010, 0b1101 ; "neg minus pos")]
    fn sub(lhs: u16, rhs: u16, expected: u16) {
        let lhs = Integer::<4>::try_from(lhs).unwrap();
        let rhs = Integer::<4>::try_from(rhs).unwrap();
        let expected = Integer::<4>::try_from(expected).unwrap();

        assert_eq!(expected, lhs - rhs, "immutable");

        let mut difference = lhs;
        difference -= rhs;

        assert_eq!(expected, difference, "sub assign");
    }

    #[test_case(0, 0 => 0 ; "0 and 0")]
    #[test_case(0, 1 => 0 ; "0 and 1")]
    #[test_case(2, 2 => 2 ; "2 and 2")]
    #[test_case(2, 3 => 2 ; "2 and 3")]
    fn bitwise_and(lhs: u16, rhs: u16) -> u16 {
        let lhs = Integer::<2>::try_from(lhs).unwrap();
        let rhs = Integer::<2>::try_from(rhs).unwrap();

        let and = lhs & rhs;
        and.into()
    }

    #[test_case(0b00 => 0b11 ; "0")]
    #[test_case(0b01 => 0b10 ; "1")]
    #[test_case(0b10 => 0b01 ; "2")]
    #[test_case(0b11 => 0b00 ; "3")]
    fn bitwise_not(value: u16) -> u16 {
        let value = Integer::<2>::try_from(value).unwrap();
        (!value).into()
    }
}
