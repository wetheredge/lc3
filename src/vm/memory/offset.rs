use std::fmt::Debug;

use crate::Integer;
use std::ops::{Sub, SubAssign};

#[derive(Clone, Copy, PartialEq)]
pub struct MemoryOffset<const BITS: u8>(Integer<BITS>);

impl MemoryOffset<16> {
    pub fn new(offset: u16) -> Self {
        Self(Integer::try_from(offset).unwrap())
    }
}

impl<const BITS: u8> Debug for MemoryOffset<BITS> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let offset = i16::from(self.0);
        write!(
            f,
            "MemoryOffset<{}>({}{:#06X})",
            BITS,
            if offset < 0 { '-' } else { '+' },
            offset.abs()
        )
    }
}

impl<const BITS: u8> From<Integer<BITS>> for MemoryOffset<BITS> {
    fn from(offset: Integer<BITS>) -> Self {
        Self(offset)
    }
}

impl<const BITS: u8> From<MemoryOffset<BITS>> for Integer<BITS> {
    fn from(offset: MemoryOffset<BITS>) -> Self {
        offset.0
    }
}

impl<const BITS: u8> Sub for MemoryOffset<BITS> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self(self.0 - rhs.0)
    }
}

impl<const BITS: u8> SubAssign for MemoryOffset<BITS> {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 = self.0 - rhs.0;
    }
}
