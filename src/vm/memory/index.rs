use std::{
    fmt::Debug,
    ops::{Add, AddAssign, Sub},
};

use super::MemoryOffset;
use crate::Integer;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct MemoryIndex(Integer<16>);

impl MemoryIndex {
    pub const fn new(index: u16) -> Self {
        Self(Integer::<16>::new(index))
    }
}

impl From<Integer<16>> for MemoryIndex {
    fn from(index: Integer<16>) -> Self {
        Self(index)
    }
}

impl From<MemoryIndex> for Integer<16> {
    fn from(index: MemoryIndex) -> Self {
        index.0
    }
}

impl From<u16> for MemoryIndex {
    fn from(index: u16) -> Self {
        Self(Integer::try_from(index).unwrap())
    }
}

impl Debug for MemoryIndex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "MemoryIndex({:#06X})", u16::from(self.0))
    }
}

impl<const BITS: u8> Add<MemoryOffset<BITS>> for MemoryIndex {
    type Output = MemoryIndex;

    fn add(self, rhs: MemoryOffset<BITS>) -> Self::Output {
        Self(self.0 + i16::from(Integer::from(rhs)).try_into().unwrap())
    }
}

impl<const BITS: u8> AddAssign<MemoryOffset<BITS>> for MemoryIndex {
    fn add_assign(&mut self, rhs: MemoryOffset<BITS>) {
        self.0 = self.0 + i16::from(Integer::from(rhs)).try_into().unwrap();
    }
}

impl Sub<MemoryIndex> for MemoryIndex {
    type Output = MemoryOffset<16>;

    fn sub(self, rhs: MemoryIndex) -> Self::Output {
        (self.0 - rhs.0).into()
    }
}

impl<const BITS: u8> Sub<MemoryOffset<BITS>> for MemoryIndex {
    type Output = MemoryIndex;

    fn sub(self, rhs: MemoryOffset<BITS>) -> Self::Output {
        let rhs: Integer<16> = Integer::<16>::try_from(u16::from(Integer::from(rhs))).unwrap();
        (self.0 - rhs).into()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;
    use test_case::test_case;

    #[test_case(0x300A, 10)]
    #[test_case(0x2FF6, -10)]
    fn add_offset(sum: u16, offset: i16) {
        self::assert_eq!(
            MemoryIndex::new(sum),
            MemoryIndex::new(0x3000) + Integer::<9>::try_from(offset).unwrap().into()
        );
    }
}
