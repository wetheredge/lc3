use std::ops::{Index, IndexMut};

use rand::{thread_rng, Rng};

use crate::Integer;

mod index;
mod offset;

pub use index::MemoryIndex;
pub use offset::MemoryOffset;

#[derive(Debug)]
pub struct Memory([Integer<16>; 0x10000]);

impl Memory {
    pub const fn new() -> Self {
        Self([Integer::<16>::new(0); 0x10000])
    }

    pub fn randomize(&mut self) {
        let mut rng = thread_rng();

        for word in self.0.iter_mut() {
            *word = rng.gen::<u16>().try_into().unwrap();
        }
    }
}

impl Default for Memory {
    fn default() -> Self {
        Self::new()
    }
}

impl Index<MemoryIndex> for Memory {
    type Output = Integer<16>;

    fn index(&self, index: MemoryIndex) -> &Self::Output {
        &self.0[u16::from(Integer::from(index)) as usize]
    }
}

impl IndexMut<MemoryIndex> for Memory {
    fn index_mut(&mut self, index: MemoryIndex) -> &mut Self::Output {
        &mut self.0[u16::from(Integer::from(index)) as usize]
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn index() {
        let mut memory = Memory::default();
        let index = MemoryIndex::new(0);
        let zero = Integer::try_from(0_u16).unwrap();
        let ten = Integer::try_from(10_u16).unwrap();

        assert_eq!(memory[index], zero);

        memory[index] = ten;
        assert_eq!(memory[index], ten);
    }
}
