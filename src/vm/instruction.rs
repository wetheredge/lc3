use crate::{
    ast::BranchFlags,
    vm::{MemoryOffset, Register},
    Integer, RuntimeError,
};
use either::Either;

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Instruction {
    ADD {
        dest: Register,
        src1: Register,
        src2: Either<Register, Integer<5>>,
    },
    AND {
        dest: Register,
        src1: Register,
        src2: Either<Register, Integer<5>>,
    },
    BR {
        flags: BranchFlags,
        to: MemoryOffset<9>,
    },
    JMP {
        to: Register,
    },
    JSR {
        offset: MemoryOffset<11>,
    },
    JSRR {
        to: Register,
    },
    LD {
        dest: Register,
        src: MemoryOffset<9>,
    },
    LDI {
        dest: Register,
        src: MemoryOffset<9>,
    },
    LDR {
        dest: Register,
        base: Register,
        offset: MemoryOffset<6>,
    },
    LEA {
        dest: Register,
        offset: MemoryOffset<9>,
    },
    NOT {
        dest: Register,
        src: Register,
    },
    RET,
    RTI,
    ST {
        src: Register,
        dest: MemoryOffset<9>,
    },
    STI {
        src: Register,
        dest: MemoryOffset<9>,
    },
    STR {
        src: Register,
        base: Register,
        offset: MemoryOffset<6>,
    },
    TRAP(Integer<8>),
}

impl Instruction {
    fn opcode(self) -> u16 {
        use Instruction::*;

        match self {
            ADD { .. } => 0b0001,
            AND { .. } => 0b0101,
            BR { .. } => 0b0000,
            JMP { .. } | RET => 0b1100,
            JSR { .. } | JSRR { .. } => 0b0100,
            LD { .. } => 0b0010,
            LDI { .. } => 0b1010,
            LDR { .. } => 0b0110,
            LEA { .. } => 0b1110,
            NOT { .. } => 0b1001,
            RTI => 0b1000,
            ST { .. } => 0b0011,
            STI { .. } => 0b1011,
            STR { .. } => 0b0111,
            TRAP(..) => 0b1111,
        }
    }

    pub fn new_trap(vector: Integer<8>) -> Self {
        Self::TRAP(vector)
    }

    pub fn new_halt() -> Self {
        Self::new_trap(0x25_u16.try_into().unwrap())
    }
}

macro_rules! impl_new {
    ($n:ident, $v:ident $(, $f:ident : $t:ty )*) => {
        impl Instruction {
            pub fn $n( $( $f : $t ),* ) -> Self {
                Self:: $v { $( $f ),* }
            }
        }
    };
}

impl_new!(
    new_add,
    ADD,
    dest: Register,
    src1: Register,
    src2: Either<Register, Integer<5>>
);
impl_new!(
    new_and,
    AND,
    dest: Register,
    src1: Register,
    src2: Either<Register, Integer<5>>
);
impl_new!(new_br, BR, flags: BranchFlags, to: MemoryOffset<9>);
impl_new!(new_jmp, JMP, to: Register);
impl_new!(new_jsr, JSR, offset: MemoryOffset<11>);
impl_new!(new_jsrr, JSRR, to: Register);
impl_new!(new_ld, LD, dest: Register, src: MemoryOffset<9>);
impl_new!(new_ldi, LDI, dest: Register, src: MemoryOffset<9>);
impl_new!(
    new_ldr,
    LDR,
    dest: Register,
    base: Register,
    offset: MemoryOffset<6>
);
impl_new!(new_lea, LEA, dest: Register, offset: MemoryOffset<9>);
impl_new!(new_not, NOT, dest: Register, src: Register);
impl_new!(new_ret, RET);
impl_new!(new_rti, RTI);
impl_new!(new_st, ST, src: Register, dest: MemoryOffset<9>);
impl_new!(new_sti, STI, src: Register, dest: MemoryOffset<9>);
impl_new!(
    new_str,
    STR,
    src: Register,
    base: Register,
    offset: MemoryOffset<6>
);

impl TryFrom<Integer<16>> for Instruction {
    type Error = RuntimeError;

    fn try_from(word: Integer<16>) -> Result<Self, Self::Error> {
        use Instruction::*;

        let instruction = u16::from(word);
        let opcode = instruction >> 12;
        let rest = instruction & (0xFFFF >> 4);

        let get_register =
            |start| Register::from(Integer::try_from(rest >> start & 0b111_u16).unwrap());
        let get_constant = || Integer::try_from(rest & 0b11111_u16).unwrap();
        let get_offset_9 = || MemoryOffset::from(Integer::try_from(rest & 0b1_1111_1111).unwrap());

        let instruction = match opcode {
            0b0001 if !rest & 0b10_0000 == 0 => ADD {
                dest: get_register(9),
                src1: get_register(6),
                src2: Either::Right(get_constant()),
            },
            0b0001 if rest & 0b11_1000 == 0 => ADD {
                dest: get_register(9),
                src1: get_register(6),
                src2: Either::Left(get_register(0)),
            },
            0b0101 if !rest & 0b10_0000 == 0 => AND {
                dest: get_register(9),
                src1: get_register(6),
                src2: Either::Right(get_constant()),
            },
            0b0101 if rest & 0b11_1000 == 0 => AND {
                dest: get_register(9),
                src1: get_register(6),
                src2: Either::Left(get_register(0)),
            },
            0b0000 => BR {
                flags: BranchFlags::new(
                    (rest >> 11) & 1 == 1,
                    (rest >> 10) & 1 == 1,
                    (rest >> 9) & 1 == 1,
                ),
                to: get_offset_9(),
            },
            0b1100 if rest == 0b0001_1100_0000 => RET,
            0b1100 if (rest | 0b1_1100_0000) == 0b0001_1100_0000 => JMP {
                to: get_register(6),
            },
            0b0100 if rest >> 11 == 1 => JSR {
                offset: MemoryOffset::from(Integer::try_from(rest & 0b111_1111_1111).unwrap()),
            },
            0b0100 if (rest | 0b1_1100_0000) == 0b0001_1100_0000 => JSRR {
                to: get_register(6),
            },
            0b0010 => LD {
                dest: get_register(9),
                src: get_offset_9(),
            },
            0b1010 => LDI {
                dest: get_register(9),
                src: get_offset_9(),
            },
            0b0110 => LDR {
                dest: get_register(9),
                base: get_register(6),
                offset: MemoryOffset::from(Integer::try_from(rest & 0b11_1111).unwrap()),
            },
            0b1110 => LEA {
                dest: get_register(9),
                offset: get_offset_9(),
            },
            0b1001 if rest.trailing_ones() >= 6 => NOT {
                dest: get_register(9),
                src: get_register(6),
            },
            0b1000 if rest == 0 => RTI,
            0b0011 => ST {
                src: get_register(9),
                dest: get_offset_9(),
            },
            0b1011 => STI {
                src: get_register(9),
                dest: get_offset_9(),
            },
            0b0111 => STR {
                src: get_register(9),
                base: get_register(6),
                offset: MemoryOffset::from(Integer::try_from(rest & 0b11_1111).unwrap()),
            },
            0b1111 if rest >> 8 == 0 => TRAP(Integer::try_from(rest & 0b1111_1111).unwrap()),
            _ => return Err(RuntimeError::MalformedInstruction),
        };

        Ok(instruction)
    }
}

impl From<Instruction> for Integer<16> {
    fn from(instruction: Instruction) -> Self {
        use Instruction::*;

        let rest: u16 = match instruction {
            ADD { dest, src1, src2 } | AND { dest, src1, src2 } if src2.is_left() => {
                (u16::from(Integer::from(dest)) << 9)
                    | (u16::from(Integer::from(src1)) << 6)
                    | u16::from(Integer::from(src2.unwrap_left()))
            }
            ADD { dest, src1, src2 } | AND { dest, src1, src2 } => {
                (u16::from(Integer::from(dest)) << 9)
                    | (u16::from(Integer::from(src1)) << 6)
                    | (1 << 5)
                    | u16::from(src2.unwrap_right())
            }
            BR { ref flags, to } => {
                (if flags.negative { 1 } else { 0 }) << 11
                    | (if flags.zero { 1 } else { 0 }) << 10
                    | (if flags.positive { 1 } else { 0 }) << 9
                    | u16::from(Integer::from(to))
            }
            JMP { to } | JSRR { to } => u16::from(Integer::from(to)) << 6,
            JSR { offset } => (1 << 11) | u16::from(Integer::from(offset)),
            LD {
                dest: register,
                src: offset,
            }
            | LDI {
                dest: register,
                src: offset,
            }
            | LEA {
                dest: register,
                offset,
            }
            | ST {
                src: register,
                dest: offset,
            }
            | STI {
                src: register,
                dest: offset,
            } => (u16::from(Integer::from(register)) << 9) | u16::from(Integer::from(offset)),
            LDR {
                dest: register1,
                base: register2,
                offset,
            }
            | STR {
                src: register1,
                base: register2,
                offset,
            } => {
                (u16::from(Integer::from(register1)) << 9)
                    | (u16::from(Integer::from(register2)) << 6)
                    | u16::from(Integer::from(offset))
            }
            NOT { dest, src } => {
                (u16::from(Integer::from(dest)) << 9)
                    | (u16::from(Integer::from(src)) << 6)
                    | 0b11_1111
            }
            RET => 0b1_1100_0000,
            RTI => 0,
            TRAP(vector) => u16::from(vector),
        };

        Self::try_from((instruction.opcode() << 12) | rest).unwrap()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ast::BranchFlags;
    use pretty_assertions::assert_eq;
    use test_case::test_case;

    use Instruction::*;
    use Register::*;

    #[test_case(
        0b0001_0000_0100_0010,
        ADD {
            dest: R0,
            src1: R1, src2:Either::Left(R2),
        };
        "ADDr"
    )]
    #[test_case(
        0b0001_0000_0110_1101,
        ADD {
            dest: R0,
            src1: R1, src2:Either::Right(Integer::try_from(0b01101_u16).unwrap()),
        };
        "ADDi"
    )]
    #[test_case(
        0b0101_0000_0100_0010,
        AND {
            dest: R0,
            src1: R1, src2:Either::Left(R2),
        };
        "ANDr"
    )]
    #[test_case(
        0b0101_0000_0110_1101,
        AND {
            dest: R0,
            src1: R1, src2:Either::Right(Integer::try_from(0b01101_u16).unwrap()),
        };
        "ANDi"
    )]
    #[test_case(
        0b0000_0110_0101_1101,
        BR {
            flags: BranchFlags {
            negative: false,
            zero: true,
            positive: true,
        },
            to: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "BR"
    )]
    #[test_case(0b1100_0000_0100_0000, JMP { to: R1 }; "JMP")]
    #[test_case(
        0b0100_1001_0111_0110,
        JSR {
            offset: MemoryOffset::from(Integer::try_from(0b001_0111_0110_u16).unwrap()),
        };
        "JSR"
    )]
    #[test_case(0b0100_0000_0100_0000, JSRR { to: R1 }; "JSRR")]
    #[test_case(
        0b0010_0000_0101_1101,
        LD {
            dest: R0,
            src: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "LD"
    )]
    #[test_case(
        0b1010_0000_0101_1101,
        LDI {
            dest: R0,
            src: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "LDI"
    )]
    #[test_case(
        0b0110_0000_0101_1101,
        LDR {
            dest: R0,
            base: R1,
            offset: MemoryOffset::from(Integer::try_from(0b01_1101_u16).unwrap()),
        };
        "LDR"
    )]
    #[test_case(
        0b1110_0000_0101_1101,
        LEA {
            dest: R0,
            offset: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "LEA"
    )]
    #[test_case(
        0b1001_0000_0111_1111,
        NOT {
            dest: R0,
            src: R1,
        };
        "NOT"
    )]
    #[test_case(0b1100_0001_1100_0000, RET; "RET")]
    #[test_case(0b1000_0000_0000_0000, RTI; "RTI")]
    #[test_case(
        0b0011_0000_0101_1101,
        ST {
            src: R0,
            dest: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "ST"
    )]
    #[test_case(
        0b1011_0000_0101_1101,
        STI {
            src: R0,
            dest: MemoryOffset::from(Integer::try_from(0b0_0101_1101_u16).unwrap()),
        };
        "STI"
    )]
    #[test_case(
        0b0111_0000_0101_1101,
        STR {
            src: R0,
            base: R1,
            offset: MemoryOffset::from(Integer::try_from(0b01_1101_u16).unwrap()),
        };
        "STR"
    )]
    #[test_case(
        0b1111_0000_0101_1100,
        TRAP(Integer::try_from(0b0101_1100_u16).unwrap());
        "TRAP"
    )]
    fn to_from_integer(word: u16, instruction: Instruction) {
        let word = Integer::try_from(word).unwrap();

        let computed = word.try_into().unwrap();

        self::assert_eq!(instruction, computed);
        self::assert_eq!(word, computed.into());
    }
}
