use either::Either;
use getset::{Getters, Setters};
use std::cmp::Ordering;

mod instruction;
mod memory;
mod registers;

pub use instruction::Instruction;
pub use memory::*;
pub use registers::Registers;

use crate::{
    assembler::{MemoryBlock, Program},
    Integer, Register, RuntimeError,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ConditionCode {
    Negative,
    Zero,
    Positive,
}

#[derive(Getters, Setters, Debug)]
pub struct VM {
    #[getset(get = "pub")]
    registers: Registers,

    #[getset(get = "pub")]
    memory: Memory,

    condition_code: ConditionCode,

    /// Program counter
    pc: MemoryIndex,
}

impl VM {
    pub const fn new() -> Self {
        Self {
            registers: Registers::new(),
            memory: Memory::new(),
            condition_code: ConditionCode::Zero,
            pc: MemoryIndex::new(0),
        }
    }

    pub fn program_counter(&self) -> &MemoryIndex {
        &self.pc
    }

    pub fn set_program_counter(&mut self, pc: MemoryIndex) -> &mut Self {
        self.pc = pc;
        self
    }

    pub fn randomize_memory(&mut self) {
        self.registers.randomize();
        self.memory.randomize();
    }

    pub fn load(&mut self, program: &Program) {
        for MemoryBlock { start, data } in &program.blocks {
            for (i, word) in data.iter().enumerate() {
                let offset = Integer::<16>::try_from(i as u16).unwrap();
                let offset = MemoryOffset::from(offset);

                self.memory[*start + offset] = *word;
            }
        }
    }

    fn advance_program_counter(&mut self) -> &mut Self {
        self.pc += MemoryOffset::from(Integer::<16>::try_from(1_u16).unwrap());
        self
    }

    fn set_condition_codes(&mut self, from: Integer<16>) {
        let condition_code = match i16::from(from).cmp(&0) {
            Ordering::Less => ConditionCode::Negative,
            Ordering::Equal => ConditionCode::Zero,
            Ordering::Greater => ConditionCode::Positive,
        };

        self.condition_code = condition_code;
    }

    pub fn step(&mut self) -> Result<(), RuntimeError> {
        use Instruction::*;
        use Register::*;

        let instruction = self.memory[self.pc];
        let instruction = Instruction::try_from(instruction)?;

        self.advance_program_counter();

        match instruction {
            ADD {
                dest,
                src1,
                src2: Either::Right(src2),
            } => {
                let result = self.registers[src1] + Integer::try_from(i16::from(src2)).unwrap();
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            ADD {
                dest,
                src1,
                src2: Either::Left(src2),
            } => {
                let result = self.registers[src1] + self.registers[src2];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            AND {
                dest,
                src1,
                src2: Either::Right(src2),
            } => {
                let result = self.registers[src1] & Integer::try_from(i16::from(src2)).unwrap();
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            AND {
                dest,
                src1,
                src2: Either::Left(src2),
            } => {
                let result = self.registers[src1] & self.registers[src2];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            BR { flags, to } => {
                if (flags.negative && self.condition_code == ConditionCode::Negative)
                    || (flags.zero && self.condition_code == ConditionCode::Zero)
                    || (flags.positive && self.condition_code == ConditionCode::Positive)
                {
                    self.pc += to;
                }
            }
            JMP { to } => self.pc = MemoryIndex::from(self.registers[to]),
            JSR { offset: to } => {
                self.registers[R7] = u16::from(Integer::from(self.pc)).try_into().unwrap();
                self.pc += to;
            }
            JSRR { to } => {
                self.registers[R7] = u16::from(Integer::from(self.pc)).try_into().unwrap();
                self.pc = self.registers[to].into();
            }
            LD { dest, src } => {
                let result = self.memory[self.pc + src];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            LDI { dest, src } => {
                let result = self.memory[self.memory[self.pc + src].into()];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            LDR { dest, base, offset } => {
                let result = self.memory[MemoryIndex::from(self.registers[base]) + offset];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            LEA { dest, offset } => {
                let result = (self.pc + offset).into();
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            NOT { dest, src } => {
                let result = !self.registers[src];
                self.set_condition_codes(result);
                self.registers[dest] = result;
            }
            RET => self.pc = self.registers[R7].into(),
            RTI => {
                let index = MemoryIndex::from(self.registers[R6]);
                self.pc = self.memory[index].into();
                // TODO: PSR = self.memory[index + MemoryOffset::new(1)];
                self.registers[R6] = (index + MemoryOffset::new(2)).into();
            }
            ST { src, dest } => self.memory[self.pc + dest] = self.registers[src],
            STI { src, dest } => {
                let index = MemoryIndex::from(self.memory[self.pc + dest]);

                self.memory[index] = self.registers[src];
            }
            STR { src, base, offset } => {
                self.memory[MemoryIndex::from(self.registers[base]) + offset] = self.registers[src];
            }
            TRAP(..) => {
                self.registers[R7] = self.pc.into();
                todo!("run trap");
                // TODO: self.program_counter = IVT[vector]
            }
        }

        Ok(())
    }

    pub fn run(&mut self) -> Result<(), RuntimeError> {
        let halt = Instruction::new_halt();
        while self.memory[self.pc].try_into() != Ok(halt) {
            self.step()?;
        }

        Ok(())
    }
}

impl Default for VM {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ast::BranchFlags;
    use pretty_assertions::assert_eq;
    use test_case::test_case;
    use Instruction::*;
    use Register::*;

    macro_rules! int {
        ($int:expr) => {{
            let int: i16 = $int;
            Integer::try_from(int).unwrap()
        }};
        (u $int:expr) => {{
            let int: u16 = $int;
            Integer::try_from(int).unwrap()
        }};
    }

    macro_rules! offset {
        ($offset:expr) => {
            MemoryOffset::from(int!($offset))
        };
    }

    fn setup(
        instruction: Instruction,
        register_state: &[(Register, i16)],
        memory_state: &[(u16, i16)],
    ) -> VM {
        let mut vm = VM::new();
        let index = MemoryIndex::new(0x3000);
        vm.memory[index] = instruction.into();
        vm.set_program_counter(index);

        for (register, value) in register_state {
            vm.registers[*register] = int!(*value);
        }

        for (index, value) in memory_state {
            vm.memory[(*index).into()] = int!(*value);
        }

        vm
    }

    #[test_case(ADD { dest: R0, src1: R0, src2: Either::Left(R1) }, &[(R0, 3), (R1, -2)], &[(R0, int!(1))] ; "ADDr")]
    #[test_case(ADD { dest: R0, src1: R0, src2: Either::Right(int!(-2)) }, &[(R0, 3)], &[(R0, int!(1))] ; "ADDi")]
    #[test_case(AND { dest: R0, src1: R0, src2: Either::Left(R1) }, &[(R0, 0b11_0111), (R1, 0b1101)], &[(R0, int!(0b00_0101))] ; "ANDr")]
    #[test_case(AND { dest: R0, src1: R0, src2: Either::Right(int!(0b1101)) }, &[(R0, 0b11_0111)], &[(R0, int!(0b00_0101))] ; "ANDi")]
    #[test_case(NOT { dest: R0, src: R0 }, &[(R0, 0x0FF0)], &[(R0, int!(u 0xF00F))] ; "NOT")]
    fn registers_only(
        instruction: Instruction,
        state: &[(Register, i16)],
        expected: &[(Register, Integer<16>)],
    ) {
        let mut vm = setup(instruction, state, &[]);

        assert!(vm.step().is_ok());

        for (register, value) in expected {
            self::assert_eq!(vm.registers()[*register], *value);
        }
    }

    #[test_case(LD { dest: R0, src: offset!(0x12) }, 123, &[], &[(0x3013, 123)] ; "LD")]
    #[test_case(LDI { dest: R0, src: offset!(0x12) }, 123, &[], &[(0x3013, 0x1234), (0x1234, 123)] ; "LDI")]
    #[test_case(LDR { dest: R0, base: R1, offset: offset!(0x12) }, 123, &[(R1, 0x4000)], &[(0x4012, 123)] ; "LDR")]
    #[test_case(LEA { dest: R0, offset: offset!(0x12) }, 0x3013, &[], &[] ; "LEA")]
    fn loadish(
        instruction: Instruction,
        expected: i16,
        register_state: &[(Register, i16)],
        memory_state: &[(u16, i16)],
    ) {
        let mut vm = setup(instruction, register_state, memory_state);

        assert!(vm.step().is_ok());
        self::assert_eq!(int!(expected), vm.registers()[R0]);
    }

    #[test_case(ST { src: R0, dest: offset!(0x12) }, (0x3013, 123), &[(R0, 123)], &[] ; "ST")]
    #[test_case(STI { src: R0, dest: offset!(0x12) }, (0x1234, 123), &[(R0, 123)], &[(0x3013, 0x1234)] ; "STI")]
    #[test_case(STR { src: R0, base: R1, offset: offset!(0x12) }, (0x4012, 123), &[(R0, 123), (R1, 0x4000)], &[] ; "STR")]
    fn store(
        instruction: Instruction,
        expected: (u16, i16),
        register_state: &[(Register, i16)],
        memory_state: &[(u16, i16)],
    ) {
        let mut vm = setup(instruction, register_state, memory_state);

        assert!(vm.step().is_ok());
        self::assert_eq!(int!(expected.1), vm.memory()[expected.0.into()]);
    }

    #[test_case(JMP { to: R0 }, 0x1234, &[(R0, 0x1234)], false ; "JMP")]
    #[test_case(JSR { offset: offset!(0x12) }, 0x3013, &[], true ; "JSR")]
    #[test_case(JSRR { to: R0 }, 0x1234, &[(R0, 0x1234)], true ; "JSRR")]
    #[test_case(RET, 0x1234, &[(R7, 0x1234)], false ; "RET")]
    fn program_counter(
        instruction: Instruction,
        expected: u16,
        register_state: &[(Register, i16)],
        set_r7: bool,
    ) {
        let mut vm = setup(instruction, register_state, &[]);

        assert!(vm.step().is_ok());
        self::assert_eq!(MemoryIndex::from(expected), *vm.program_counter());

        if set_r7 {
            self::assert_eq!(int!(0x3001), vm.registers()[R7]);
        }
    }

    #[test_case(true, true, true ; "nzp")]
    #[test_case(true, true, false ; "nz")]
    #[test_case(true, false, true ; "np")]
    #[test_case(false, true, true ; "zp")]
    #[test_case(true, false, false ; "n")]
    #[test_case(false, true, false ; "z")]
    #[test_case(false, false, true ; "p")]
    #[test_case(false, false, false ; "none")]
    fn branch(negative: bool, zero: bool, positive: bool) {
        let index = MemoryIndex::new(0x3000);
        let instruction = BR {
            flags: BranchFlags {
                negative,
                zero,
                positive,
            },
            to: offset!(0x12),
        };

        for (condition_code, branch_expected) in &[
            (ConditionCode::Negative, negative),
            (ConditionCode::Zero, zero),
            (ConditionCode::Positive, positive),
        ] {
            let expected = if *branch_expected { 0x3013 } else { 0x3001 };
            let expected = MemoryIndex::from(expected);

            let mut vm = VM::new();
            vm.memory[index] = instruction.into();
            vm.set_program_counter(index);

            vm.condition_code = *condition_code;

            assert!(vm.step().is_ok());
            self::assert_eq!(expected, *vm.program_counter());
        }
    }
}
