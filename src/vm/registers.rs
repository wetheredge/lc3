use rand::{thread_rng, Rng};
use std::ops::{Index, IndexMut};

use crate::{Integer, Register};

#[derive(Debug, Default)]
pub struct Registers([Integer<16>; 8]);

impl Registers {
    pub const fn new() -> Self {
        Self([Integer::<16>::new(0); 8])
    }

    pub fn randomize(&mut self) {
        let mut rng = thread_rng();

        for register in &mut self.0 {
            *register = rng.gen::<u16>().try_into().unwrap();
        }
    }
}

impl Index<Register> for Registers {
    type Output = Integer<16>;

    fn index(&self, register: Register) -> &Self::Output {
        &self.0[u16::from(Integer::from(register)) as usize]
    }
}

impl IndexMut<Register> for Registers {
    fn index_mut(&mut self, register: Register) -> &mut Self::Output {
        &mut self.0[u16::from(Integer::from(register)) as usize]
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn index() {
        let mut registers = Registers::default();
        let zero = Integer::try_from(0_u16).unwrap();
        let ten = Integer::try_from(10_u16).unwrap();

        assert_eq!(registers[Register::R0], zero);
        registers[Register::R0] = ten;
        assert_eq!(registers[Register::R0], ten);
    }
}
