use crate::vm;
use crate::{Integer, Register};
use either::Either;
use strum_macros::{EnumString, EnumVariantNames};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct BranchFlags {
    pub negative: bool,
    pub zero: bool,
    pub positive: bool,
}

impl BranchFlags {
    pub fn new(negative: bool, zero: bool, positive: bool) -> Self {
        Self {
            negative,
            zero,
            positive,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum MemoryOffset<const BITS: u8> {
    Literal(vm::MemoryOffset<BITS>),
    Label(String),
}

#[derive(Debug, Clone, PartialEq)]
#[allow(clippy::upper_case_acronyms)]
pub enum Instruction {
    ADD {
        dest: Register,
        src1: Register,
        src2: Either<Register, Integer<5>>,
    },
    AND {
        dest: Register,
        src1: Register,
        src2: Either<Register, Integer<5>>,
    },
    BR {
        flags: BranchFlags,
        to: MemoryOffset<9>,
    },
    JMP {
        to: Register,
    },
    JSR {
        offset: MemoryOffset<11>,
    },
    JSRR {
        to: Register,
    },
    LD {
        dest: Register,
        src: MemoryOffset<9>,
    },
    LDI {
        dest: Register,
        src: MemoryOffset<9>,
    },
    LDR {
        dest: Register,
        base: Register,
        offset: MemoryOffset<6>,
    },
    LEA {
        dest: Register,
        offset: MemoryOffset<9>,
    },
    NOT {
        dest: Register,
        src: Register,
    },
    RET,
    RTI,
    ST {
        src: Register,
        dest: MemoryOffset<9>,
    },
    STI {
        src: Register,
        dest: MemoryOffset<9>,
    },
    STR {
        src: Register,
        base: Register,
        offset: MemoryOffset<6>,
    },
    TRAP(Integer<8>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, EnumString, EnumVariantNames)]
#[allow(clippy::upper_case_acronyms)]
#[strum(ascii_case_insensitive)]
pub enum InstructionName {
    ADD,
    AND,
    BR(BranchFlags),
    JMP,
    JSR,
    JSRR,
    LD,
    LDI,
    LDR,
    LEA,
    NOT,
    RET,
    RTI,
    ST,
    STI,
    STR,
    TRAP,
}
