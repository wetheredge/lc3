mod instruction;

use crate::vm::MemoryIndex;
use crate::Integer;
pub use instruction::{BranchFlags, Instruction, InstructionName, MemoryOffset};
use strum_macros::{EnumDiscriminants, EnumString, EnumVariantNames};

#[derive(Debug, Clone, PartialEq, EnumDiscriminants)]
#[allow(clippy::upper_case_acronyms)]
#[strum_discriminants(name(PseudoOpName))]
#[strum_discriminants(derive(Hash, EnumString, EnumVariantNames))]
#[strum_discriminants(strum(ascii_case_insensitive))]
pub enum PseudoOp {
    ORIG(MemoryIndex),
    END,
    FILL(Integer<16>),
    BLKW(usize),
    STRINGZ(String),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, EnumString, EnumVariantNames)]
#[allow(clippy::upper_case_acronyms)]
#[strum(ascii_case_insensitive)]
pub enum TrapName {
    GETC,
    OUT,
    PUTS,
    IN,
    PUTSP,
    HALT,
}

impl From<TrapName> for Instruction {
    fn from(trap: TrapName) -> Self {
        match trap {
            TrapName::GETC => Instruction::TRAP(Integer::<8>::new(0x20)),
            TrapName::OUT => Instruction::TRAP(Integer::<8>::new(0x21)),
            TrapName::PUTS => Instruction::TRAP(Integer::<8>::new(0x22)),
            TrapName::IN => Instruction::TRAP(Integer::<8>::new(0x23)),
            TrapName::PUTSP => Instruction::TRAP(Integer::<8>::new(0x24)),
            TrapName::HALT => Instruction::TRAP(Integer::<8>::new(0x25)),
        }
    }
}

#[derive(Debug)]
pub enum DirectiveName {
    Instruction(InstructionName),
    PseudoOp(PseudoOpName),
    NamedTrap(TrapName),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Directive {
    Instruction(Instruction),
    PseudoOp(PseudoOp),
}
