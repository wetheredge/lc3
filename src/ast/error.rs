use super::{NumberLiteralBase, OperandKind, RangeError, Span, TokenKind};

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct ParseError {
    pub span: Span,
    pub kind: ParseErrorKind,
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub enum ParseErrorKind {
    ExpectedToken {
        found: TokenKind,
        expected: Vec<TokenKind>,
    },
    ExtraTokens,
    InvalidBranchFlags,
    InvalidDigit {
        base: NumberLiteralBase,
    },
    InvalidInstruction,
    InvalidOperandKind {
        found: TokenKind,
        expected: OperandKind,
    },
    InvalidPseudoOp,
    LiteralOutOfRange(RangeError),
    MissingOperandSeparator,
    TooFewOperands {
        found: usize,
        expected: usize,
    },
    UnclosedString,
    UnexpectedCharacter,
}
