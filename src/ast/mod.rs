mod directive;
mod parse;

pub(crate) use directive::{BranchFlags, Directive, Instruction, MemoryOffset, PseudoOp};
use parse::parse_line;
pub use parse::{OperandKind, RangeError, TokenKind};
use std::fmt;
use std::ops::Add;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum NumberLiteralBase {
    Binary,
    Decimal,
    Hexadecimal,
}

impl NumberLiteralBase {
    pub fn from_token(token: &str) -> Option<Self> {
        match token.chars().next() {
            Some('b' | 'B') => Some(Self::Binary),
            Some('#') => Some(Self::Decimal),
            Some('x' | 'X') => Some(Self::Hexadecimal),
            _ => None,
        }
    }
}

impl From<NumberLiteralBase> for u32 {
    fn from(base: NumberLiteralBase) -> Self {
        match base {
            NumberLiteralBase::Binary => 2,
            NumberLiteralBase::Decimal => 10,
            NumberLiteralBase::Hexadecimal => 16,
        }
    }
}

impl fmt::Display for NumberLiteralBase {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Binary => "binary",
                Self::Decimal => "decimal",
                Self::Hexadecimal => "hexadecimal",
            }
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Hash)]
pub struct Span {
    pub start: usize,
    pub end: usize,
}

impl Span {
    pub fn new(start: usize, end: usize) -> Self {
        Self { start, end }
    }

    pub fn single_char(char: usize) -> Self {
        Self::from_length(char, 1)
    }

    pub fn from_length(start: usize, length: usize) -> Self {
        Self {
            start,
            end: start + length,
        }
    }
}

impl Add<usize> for Span {
    type Output = Self;

    fn add(self, rhs: usize) -> Self::Output {
        Self::new(self.start + rhs, self.end + rhs)
    }
}

impl From<Span> for std::ops::Range<usize> {
    fn from(span: Span) -> Self {
        span.start..span.end
    }
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct ParseError {
    pub span: Span,
    pub kind: ParseErrorKind,
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub enum ParseErrorKind {
    ExpectedToken {
        found: TokenKind,
        expected: Vec<TokenKind>,
    },
    ExtraTokens,
    InvalidBranchFlags,
    InvalidDigit {
        base: NumberLiteralBase,
    },
    InvalidInstruction(String),
    InvalidOperandKind {
        found: TokenKind,
        expected: OperandKind,
    },
    InvalidPseudoOp(String),
    LiteralOutOfRange(RangeError),
    MissingOperandSeparator,
    TooFewOperands {
        found: usize,
        expected: usize,
    },
    UnclosedString,
    UnexpectedCharacter,
}

#[derive(Debug)]
pub struct Line {
    pub label: Option<(usize, String)>,
    pub directive: Option<(usize, Directive)>,
    pub comment: Option<(usize, String)>,
}

impl Line {
    fn empty() -> Self {
        Self {
            label: None,
            directive: None,
            comment: None,
        }
    }
}

#[derive(Debug)]
pub struct Ast(Vec<Line>);

impl Ast {
    pub fn parse(src: &str) -> Result<Self, Vec<ParseError>> {
        let mut lines = Vec::new();
        let mut errs = Vec::new();

        let mut line_offset = 0;
        // for (line_number, line) in src.split_terminator('\n').enumerate() {
        for line in src.split_terminator('\n') {
            let current_offset = line_offset;
            line_offset += line.len() + 1;
            let line = line.strip_suffix('\r').unwrap_or(line);

            match parse_line(line) {
                Ok(line) => lines.push(line),
                Err(new_errs) => new_errs.iter().for_each(|ParseError { span, kind }| {
                    errs.push(ParseError {
                        span: *span + current_offset,
                        kind: kind.clone(),
                    });
                }),
            }
        }

        if errs.is_empty() {
            Ok(Self(lines))
        } else {
            Err(errs)
        }
    }
}

impl IntoIterator for Ast {
    type Item = Line;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
