mod instruction;
mod integer;
mod register;
mod string;
mod tokenization;

use super::directive::{
    Directive, DirectiveName, Instruction, InstructionName, MemoryOffset, PseudoOp, PseudoOpName,
    TrapName,
};
use super::{Line, ParseError, ParseErrorKind, Span};
use either::Either;
use std::fmt;
use std::str::FromStr;
use tokenization::{classify_token, split_tokens};

pub use integer::RangeError;
pub use tokenization::TokenKind;

macro_rules! count {
    () => (0);
    ($first:expr) => (1);
    ($first:expr , $( $rest:expr ),+) => (1 + count!( $( $rest ),+ ));
}

#[derive(Debug, Clone, Copy, PartialEq, Hash)]
pub enum OperandKind {
    Immediate,
    Offset,
    Register,
    SignedNumber,
    String,
    TrapVector,
    UnsignedNumber,
}

impl fmt::Display for OperandKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Immediate => write!(f, "register or literal"),
            Self::Offset => write!(f, "offset"),
            Self::Register => write!(f, "register"),
            Self::SignedNumber => write!(f, "signed number"),
            Self::String => write!(f, "string"),
            Self::TrapVector => write!(f, "trap vector"),
            Self::UnsignedNumber => write!(f, "unsigned number"),
        }
    }
}

type Token<'a> = (usize, &'a str, TokenKind);

fn comma_check(tokens: &[Token], next_token: usize) -> Result<usize, ParseError> {
    match tokens.get(next_token) {
        Some((_, _, TokenKind::Comma)) => Ok(next_token + 1),
        Some(&(start, token, kind)) if !kind.can_be_operand() => Err(ParseError {
            span: Span::from_length(start, token.len()),
            kind: ParseErrorKind::ExpectedToken {
                found: kind,
                expected: vec![TokenKind::Comma],
            },
        }),
        _ => {
            if tokens.get(next_token + 1).is_none() {
                // The next operand after this expected comma is missing too, so continue so the
                // error for the missing operand can be returned instead
                Ok(next_token)
            } else {
                let last_token = tokens[next_token - 1];
                Err(ParseError {
                    span: Span::single_char(last_token.0 + last_token.1.len()),
                    kind: ParseErrorKind::MissingOperandSeparator,
                })
            }
        }
    }
}

fn get_error(
    tokens: &[Token],
    current_token: usize,
    err: Option<&Token>,
    expected_kind: OperandKind,
    expected_count: usize,
) -> ParseError {
    match err {
        Some((start, token, kind)) => ParseError {
            span: Span::from_length(*start, token.len()),
            kind: ParseErrorKind::InvalidOperandKind {
                found: *kind,
                expected: expected_kind,
            },
        },
        None => {
            let (last_valid_start, last_valid_token, _) = tokens[current_token - 1];
            let (found, directive) = tokens
                .iter()
                .enumerate()
                .take(current_token)
                .rev()
                .take_while(|(_, (_, _, kind))| {
                    !matches!(kind, TokenKind::Instruction | TokenKind::PseudoOp)
                })
                .filter(|(_, (_, _, kind))| kind.can_be_operand())
                .fold((0, 0), |(found, _), (i, _)| (found + 1, i));

            let end = last_valid_start + last_valid_token.len();
            let start = tokens[if found > 0 { directive - 1 } else { directive }].0;

            ParseError {
                span: Span::new(start, end),
                // span: Span::from_length(last_valid_start, last_valid_token.len()),
                kind: ParseErrorKind::TooFewOperands {
                    expected: expected_count,
                    found,
                },
            }
        }
    }
}

pub(super) fn parse_line(line: &str) -> Result<Line, Vec<ParseError>> {
    let tokens = split_tokens(line)?;
    let mut tokens = tokens
        .iter()
        .map(|(start, token)| {
            let kind = classify_token(*token).map_err(|(at, kind)| {
                let span = if let Some(at) = at {
                    Span::single_char(at)
                } else {
                    Span::from_length(0, token.len())
                };
                let span = span + *start;
                vec![ParseError { span, kind }]
            })?;
            Ok((*start, *token, kind))
        })
        .collect::<Result<Vec<_>, Vec<_>>>()?;

    if tokens.is_empty() {
        return Ok(Line::empty());
    }

    let comment = if tokens[tokens.len() - 1].2 == TokenKind::Comment {
        let (start, comment, _) = tokens.pop().unwrap();
        Some((start, comment.to_string()))
    } else {
        None
    };

    let mut next_token = 0;

    let label = if let Some((start, label, TokenKind::Label)) = tokens.get(next_token) {
        let start = *start;
        let label = label.to_string();

        next_token += 1;

        if let Some((_, _, TokenKind::Colon)) = tokens.get(next_token) {
            next_token += 1;
        }

        Some((start, label))
    } else {
        None
    };

    let directive = match tokens.get(next_token) {
        Some((start, token, TokenKind::Instruction)) => Ok(Some((
            *start,
            DirectiveName::Instruction(instruction::parse_name(token)),
        ))),
        Some((start, token, TokenKind::NamedTrap)) => Ok(Some((
            *start,
            DirectiveName::NamedTrap(TrapName::from_str(token).unwrap()),
        ))),
        Some((start, token, TokenKind::PseudoOp)) => Ok(Some((
            *start,
            DirectiveName::PseudoOp(
                PseudoOpName::from_str(token.strip_prefix('.').unwrap()).unwrap(),
            ),
        ))),
        Some((start, got, TokenKind::Label)) => Err(ParseError {
            span: Span::from_length(*start, got.len()),
            kind: ParseErrorKind::InvalidInstruction(got.to_string()),
        }),
        Some((start, token, kind)) => Err(ParseError {
            span: Span::from_length(*start, token.len()),
            kind: ParseErrorKind::ExpectedToken {
                found: *kind,
                expected: vec![TokenKind::Instruction, TokenKind::PseudoOp],
            },
        }),
        None => Ok(None),
    };
    let directive = directive.map_err(|err| vec![err])?;

    next_token += 1;

    let mut parsed_first_operand = false;
    macro_rules! get_operand {
        (immediate, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((_, token, TokenKind::Register)) => Ok(Either::Left(register::parse(token))),
                Some((start, token, TokenKind::Number)) => {
                    Ok(Either::Right(integer::parse(*start, token, true)?))
                }
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    OperandKind::Immediate,
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};

        (offset, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((start, token, TokenKind::Number)) => Ok(MemoryOffset::Literal(
                    integer::parse(*start, token, true).unwrap().into(),
                )),
                Some((_, token, TokenKind::Label)) => Ok(MemoryOffset::Label(token.to_string())),
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    OperandKind::Offset,
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};

        (register, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((_, token, TokenKind::Register)) => Ok(register::parse(token)),
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    OperandKind::Register,
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};

        (signed number, $count:expr) => {
            get_operand!(number, true, $count)
        };
        (unsigned number, $count:expr) => {
            get_operand!(number, false, $count)
        };
        (number, $signed:literal, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((start, token, TokenKind::Number)) => {
                    Ok(integer::parse::<16>(*start, token, $signed)?)
                }
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    if $signed {
                        OperandKind::SignedNumber
                    } else {
                        OperandKind::UnsignedNumber
                    },
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};

        (string, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((_, token, TokenKind::String)) => Ok(string::parse(token)),
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    OperandKind::String,
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};

        (trap vector, $count:expr) => {{
            if parsed_first_operand {
                next_token = comma_check(&tokens, next_token)?;
            }

            parsed_first_operand = true;

            let operand = match tokens.get(next_token) {
                Some((start, token, TokenKind::Number)) => {
                    Ok(integer::parse(*start, token, false)?)
                }
                err => Err(get_error(
                    &tokens,
                    next_token,
                    err,
                    OperandKind::TrapVector,
                    $count,
                )),
            };

            if operand.is_ok() {
                next_token += 1;
            }

            operand
        }};
    }

    macro_rules! instruction {
        ($instruction:ident { $($field:ident : $type:ident),* $(,)? }) => {{
            let count: usize = { count!( $( $field ),* ) };
            $( let $field = get_operand!($type, count)?; )*
            Instruction::$instruction {
                $( $field ),*
            }
        }};
    }

    let directive = directive.map(|(start, directive)| {
        #[allow(clippy::eval_order_dependence)]
        let directive = match directive {
            DirectiveName::Instruction(instruction) => Directive::Instruction(match instruction {
                InstructionName::ADD => instruction!(ADD {
                    dest: register,
                    src1: register,
                    src2: immediate,
                }),
                InstructionName::AND => instruction!(AND {
                    dest: register,
                    src1: register,
                    src2: immediate,
                }),
                InstructionName::BR(flags) => Instruction::BR {
                    flags,
                    to: get_operand!(offset, 1)?,
                },
                InstructionName::JMP => instruction!(JMP { to: register }),
                InstructionName::JSR => instruction!(JSR { offset: offset }),
                InstructionName::JSRR => instruction!(JSRR { to: register }),
                InstructionName::LD => instruction!(LD {
                    dest: register,
                    src: offset,
                }),
                InstructionName::LDI => instruction!(LDI {
                    dest: register,
                    src: offset,
                }),
                InstructionName::LDR => instruction!(LDR {
                    dest: register,
                    base: register,
                    offset: offset,
                }),
                InstructionName::LEA => instruction!(LEA {
                    dest: register,
                    offset: offset,
                }),
                InstructionName::NOT => instruction!(NOT {
                    dest: register,
                    src: register,
                }),
                InstructionName::RET => Instruction::RET,
                InstructionName::RTI => Instruction::RTI,
                InstructionName::ST => instruction!(ST {
                    src: register,
                    dest: offset,
                }),
                InstructionName::STI => instruction!(STI {
                    src: register,
                    dest: offset,
                }),
                InstructionName::STR => instruction!(STR {
                    src: register,
                    base: register,
                    offset: offset,
                }),
                InstructionName::TRAP => Instruction::TRAP(get_operand!(trap vector, 1)?),
            }),
            DirectiveName::NamedTrap(trap) => Directive::Instruction(trap.into()),
            DirectiveName::PseudoOp(pseudo_op) => Directive::PseudoOp(match pseudo_op {
                PseudoOpName::BLKW => {
                    PseudoOp::BLKW(u16::from(get_operand!(unsigned number, 1)?) as usize)
                }
                PseudoOpName::END => PseudoOp::END,
                PseudoOpName::FILL => PseudoOp::FILL(get_operand!(signed number, 1)?),
                PseudoOpName::ORIG => PseudoOp::ORIG(get_operand!(unsigned number, 1)?.into()),

                PseudoOpName::STRINGZ => PseudoOp::STRINGZ(get_operand!(string, 1)?),
            }),
        };

        Ok((start, directive))
    });
    let directive = match directive {
        Some(Ok(directive)) => Ok(Some(directive)),
        Some(Err(err)) => Err(vec![err]),
        None => Ok(None),
    };
    let directive = directive?;

    let remaining_tokens = tokens.iter().skip(next_token).collect::<Vec<_>>();

    if !remaining_tokens.is_empty() {
        let last = remaining_tokens.last().unwrap();
        Err(vec![ParseError {
            span: Span::new(remaining_tokens[0].0, last.0 + last.1.len()),
            kind: ParseErrorKind::ExtraTokens,
        }])
    } else {
        Ok(Line {
            label,
            directive,
            comment,
        })
    }
}
