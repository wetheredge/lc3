use crate::ast::{NumberLiteralBase, ParseError, ParseErrorKind, Span};
use crate::Integer;
use std::fmt;
use std::num::IntErrorKind;

#[derive(Debug, Clone, Copy, PartialEq, Hash)]
pub enum RangeError {
    MustBePositive,
    TooWide { available: usize },
}

impl fmt::Display for RangeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::MustBePositive => write!(f, "must be positive"),
            Self::TooWide { available } => write!(f, "cannot fit in {available} bits"),
        }
    }
}

pub(super) fn parse<const BITS: u8>(
    start: usize,
    token: &str,
    can_be_negative: bool,
) -> Result<Integer<BITS>, ParseError> {
    let base = NumberLiteralBase::from_token(token).unwrap();

    let augment_err = |err| ParseError {
        span: Span::from_length(start, token.len()),
        kind: ParseErrorKind::LiteralOutOfRange(err),
    };

    let has_negative_sign = token.contains('-');
    if !can_be_negative && has_negative_sign {
        return Err(augment_err(RangeError::MustBePositive));
    }

    let signs: &[char] = &['-', '+'];
    let number = token[1..].replace(signs, "");

    let number = u32::from_str_radix(&number, base.into()).map_err(|err| {
        if *err.kind() == IntErrorKind::PosOverflow {
            augment_err(RangeError::TooWide {
                available: BITS.into(),
            })
        } else {
            unreachable!()
        }
    })?;

    let bits = u32::BITS - number.leading_zeros();

    if bits > BITS.into() {
        Err(augment_err(RangeError::TooWide {
            available: BITS.into(),
        }))
    } else {
        let integer = if has_negative_sign {
            Integer::try_from(-(number as i32) as i16).unwrap()
        } else {
            Integer::try_from(number as u16).unwrap()
        };

        Ok(integer)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::proptest;
    use test_case::test_case;

    macro_rules! parse_test {
        ($base_name:literal, $format:literal, $signed:literal, $value:expr, $expected:expr) => {{
            let s = format!($format, $value);
            let parsed = parse::<16>(0, &s, $signed);
            assert_eq!(Ok($expected), parsed, $base_name);
        }};
    }

    proptest! {
        #[test]
        fn negative(value in Integer::<16>::SIGNED_MIN..0) {
            let expected = Integer::try_from(value).unwrap();
            let value = (value as i32).abs();

            parse_test!("binary",  "b-{:b}", true, value, expected);
            parse_test!("decimal", "#-{}",   true, value, expected);
            parse_test!("hex",     "x-{:x}", true, value, expected);
        }

        #[test]
        fn positive(value in 0..Integer::<16>::MAX) {
            let expected = Integer::try_from(value).unwrap();

            parse_test!("binary",  "b{:b}", false, value, expected);
            parse_test!("decimal", "#{}",   false, value, expected);
            parse_test!("hex",     "x{:x}", false, value, expected);
        }

        #[test]
        fn signed_positive(value in 0..Integer::<16>::SIGNED_MAX) {
            let expected = Integer::try_from(value).unwrap();

            parse_test!("binary",  "b+{:b}", true, value, expected);
            parse_test!("decimal", "#+{}",   true, value, expected);
            parse_test!("hex",     "x+{:x}", true, value, expected);
        }
    }

    #[test]
    fn unsigned_full_16_bits() {
        assert_eq!(
            Ok(Integer::try_from(0xFFFF_u16).unwrap()),
            parse::<16>(0, "xFFFF", false)
        );
    }

    #[test]
    fn signed_full_16_bits() {
        assert_eq!(
            Ok(Integer::try_from(-0x8000_i16).unwrap()),
            parse::<16>(0, "x-8000", true)
        );
    }

    #[test]
    fn unsigned_full_4_bits() {
        assert_eq!(
            Ok(Integer::try_from(0xF_u16).unwrap()),
            parse::<4>(0, "xF", false)
        );
    }

    #[test]
    fn signed_full_4_bits() {
        assert_eq!(
            Ok(Integer::try_from(-0x8_i16).unwrap()),
            parse::<4>(0, "x-8", true)
        );
    }

    #[test_case("xFFFF", false ; "unsigned overflow")]
    #[test_case("x-8000", true ; "signed overflow")]
    #[test_case("xFFFFFFFFFFFF", false ; "unsigned 32 bit overflow")]
    #[test_case("x-800000000000", true ; "signed 32 bit overflow")]
    fn error(input: &str, signed: bool) {
        assert_eq!(
            ParseErrorKind::LiteralOutOfRange(RangeError::TooWide { available: 15 }),
            parse::<15>(0, input, signed).unwrap_err().kind
        );
    }

    #[test]
    fn disallowed_negative() {
        assert_eq!(
            ParseErrorKind::LiteralOutOfRange(RangeError::MustBePositive),
            parse::<16>(0, "#-1", false).unwrap_err().kind
        );
    }
}
