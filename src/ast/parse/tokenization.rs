use super::{InstructionName, PseudoOpName, TrapName};
use crate::ast::{NumberLiteralBase, ParseError, ParseErrorKind, Span};
use std::fmt;
use strum::VariantNames;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TokenKind {
    Colon,
    Comma,
    Comment,
    Instruction,
    NamedTrap,
    Label,
    Number,
    PseudoOp,
    Register,
    String,
}

impl TokenKind {
    pub(crate) fn can_be_operand(self) -> bool {
        matches!(
            self,
            Self::Label | Self::Number | Self::Register | Self::String
        )
    }
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Colon => write!(f, "colon"),
            Self::Comma => write!(f, "comma"),
            Self::Comment => write!(f, "comment"),
            Self::Instruction => write!(f, "instruction"),
            Self::NamedTrap => write!(f, "named trap"),
            Self::Label => write!(f, "label"),
            Self::Number => write!(f, "number"),
            Self::PseudoOp => write!(f, "pseudo op"),
            Self::Register => write!(f, "register"),
            Self::String => write!(f, "string"),
        }
    }
}

fn check_branch_flags(mut flags: &str) -> Result<(), (Option<usize>, ParseErrorKind)> {
    let err = |at| Err((Some(at), ParseErrorKind::InvalidBranchFlags));
    let mut at = 2;

    match flags.chars().next() {
        Some('N') => {
            flags = flags.strip_prefix('N').unwrap();
            at += 1;
        }
        Some('Z' | 'P') => {}
        None => return Ok(()),
        _ => return err(at),
    }

    match flags.chars().next() {
        Some('Z') => {
            flags = flags.strip_prefix('Z').unwrap();
            at += 1;
        }
        Some('P') => {}
        None => return Ok(()),
        _ => return err(at),
    }

    match flags.chars().next() {
        Some('P') => {
            flags = flags.strip_prefix('P').unwrap();
            at += 1;
        }
        None => return Ok(()),
        _ => return err(at),
    }

    if flags.is_empty() {
        Ok(())
    } else {
        err(at)
    }
}

pub fn split_tokens(line: &str) -> Result<Vec<(usize, &str)>, Vec<ParseError>> {
    let mut tokens = Vec::new();
    let mut errors = Vec::new();

    let mut start = None;
    let mut in_string = false;
    let mut escaped_char = false;

    let mut chars = line.char_indices().peekable();
    while let Some((i, char)) = chars.next() {
        match (char, chars.peek().map(|(_, c)| c)) {
            (' ' | '\t', _) if !in_string => {
                debug_assert!(start.is_none());
            }
            (',' | ':', _) => {
                tokens.push((i, &line[i..=i]));
            }
            ('"', Some(_)) if !in_string => {
                start = Some(i);
                in_string = true;
            }
            ('"', _) if in_string && !escaped_char => {
                tokens.push((start.unwrap(), &line[start.unwrap()..=i]));
                start = None;
                in_string = false;
            }
            ('"', None) if start.is_none() => errors.push(ParseError {
                span: Span::single_char(i),
                kind: ParseErrorKind::UnclosedString,
            }),
            (_, None) if in_string => errors.push(ParseError {
                span: Span::new(start.unwrap(), i + 1),
                kind: ParseErrorKind::UnclosedString,
            }),
            ('\\', _) if in_string => escaped_char = true,
            (_, _) if in_string => escaped_char = false,
            (';', _) => {
                tokens.push((i, &line[i..line.len()]));
                break;
            }
            (_, None | Some(' ' | '\t' | ',' | ':' | '"' | ';')) => {
                let start_index = start.unwrap_or(i);
                tokens.push((start_index, &line[start_index..=i]));
                start = None;
            }
            ('\n' | '\r', _) | (_, Some('\n' | '\r')) => unreachable!(),
            (_, _) => {
                if start.is_none() {
                    start = Some(i);
                }
            }
        }
    }

    if errors.is_empty() {
        Ok(tokens)
    } else {
        Err(errors)
    }
}

fn is_register(token: &str) -> bool {
    token.len() == 2
        && (token.starts_with('r') || token.starts_with('R'))
        && token.ends_with(|c| ('0'..='7').contains(&c))
}

pub fn classify_token(token: &str) -> Result<TokenKind, (Option<usize>, ParseErrorKind)> {
    if token == "," {
        return Ok(TokenKind::Comma);
    } else if token == ":" {
        return Ok(TokenKind::Colon);
    } else if token.starts_with(';') {
        return Ok(TokenKind::Comment);
    }
    if token.starts_with('"') {
        return Ok(TokenKind::String);
    } else if is_register(token) {
        return Ok(TokenKind::Register);
    } else if let Some(rest) = token.strip_prefix('.') {
        for (at, char) in rest.char_indices().map(|(i, char)| (i + 1, char)) {
            if !char.is_alphabetic() {
                return Err((Some(at), ParseErrorKind::UnexpectedCharacter));
            }
        }

        return if PseudoOpName::VARIANTS.contains(&rest.to_uppercase().as_str()) {
            Ok(TokenKind::PseudoOp)
        } else {
            Err((None, ParseErrorKind::InvalidPseudoOp(token.to_string())))
        };
    }

    if let Some(rest) = token.strip_prefix(|c| ['x', 'X', '#', 'b', 'B'].contains(&c)) {
        if !rest.is_empty() {
            let base = NumberLiteralBase::from_token(token).unwrap();

            let err = rest
                .char_indices()
                .find_map(|(i, char)| match (i + 1, char, base) {
                    (1, '-' | '+', _)
                    | (_, '0' | '1', NumberLiteralBase::Binary)
                    | (_, '0'..='9', NumberLiteralBase::Decimal)
                    | (_, '0'..='9' | 'a'..='f' | 'A'..='F', NumberLiteralBase::Hexadecimal) => {
                        None
                    }
                    (at, '0'..='9' | 'a'..='f' | 'A'..='F', _) => Some(Some(at)),
                    _ => Some(None),
                });

            match err {
                Some(Some(at)) => return Err((Some(at), ParseErrorKind::InvalidDigit { base })),
                Some(None) => {}
                None => return Ok(TokenKind::Number),
            }
        }
    }

    let uppercase = token.to_uppercase();
    let uppercase = uppercase.as_str();
    if InstructionName::VARIANTS.contains(&uppercase) {
        Ok(TokenKind::Instruction)
    } else if TrapName::VARIANTS.contains(&uppercase) {
        Ok(TokenKind::NamedTrap)
    } else if let Some(flags) = uppercase.strip_prefix("BR") {
        check_branch_flags(flags).map(|_| TokenKind::Instruction)
    } else {
        check_for_confusing_label(token);
        Ok(TokenKind::Label)
    }
}

fn check_for_confusing_label(token: &str) {
    let uppercase = token.to_uppercase();

    if uppercase.starts_with('R') && token.chars().skip(1).all(|c| c.is_ascii_digit()) {
        println!("warning: label {token} looks like a register");
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn unclosed_empty_string() {
        assert_eq!(
            Err(vec![ParseError {
                kind: ParseErrorKind::UnclosedString,
                span: Span::single_char(0)
            }]),
            split_tokens("\"")
        );
    }

    #[test]
    fn empty_string() {
        assert_eq!(Ok(vec![(0, r#""""#)]), split_tokens(r#""""#));
    }

    #[test]
    fn string_escaped_chars() {
        let string = r#""\"\\\n\r\t\0""#;
        assert_eq!(Ok(vec![(0, string)]), split_tokens(string));
    }

    #[test]
    fn string_unneeded_escape() {
        let string = r#""\ \a""#;
        assert_eq!(Ok(vec![(0, string)]), split_tokens(string));
    }

    #[test]
    fn string_only_escaped_space() {
        let string = r#""\ ""#;
        assert_eq!(Ok(vec![(0, string)]), split_tokens(string));
    }

    #[test]
    fn single_char_label() {
        assert_eq!(Ok(vec![(0, "a")]), split_tokens("a"));
    }

    #[test]
    fn lone_b_is_label_not_number() {
        assert_eq!(Ok(TokenKind::Label), classify_token("b"));
    }
}
