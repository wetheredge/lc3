use super::InstructionName;
use crate::ast::directive::BranchFlags;
use std::str::FromStr;

pub(super) fn parse_name(token: &str) -> InstructionName {
    match token.to_uppercase().as_str() {
        instruction if instruction.starts_with("BR") => {
            let flags = instruction.strip_prefix("BR").unwrap();
            let n = flags.contains('N');
            let z = flags.contains('Z');
            let p = flags.contains('P');

            InstructionName::BR(BranchFlags::new(n, z, p))
        }
        token => InstructionName::from_str(token).unwrap(),
    }
}
