pub(super) fn parse(token: &str) -> String {
    token
        .strip_prefix('"')
        .unwrap()
        .strip_suffix('"')
        .unwrap()
        .replace("\\t", "\t")
        .replace("\\n", "\n")
        .replace("\\r", "\r")
        .replace("\\\"", "\"")
}
