use clap::{ArgEnum, Parser};
use codespan_reporting::diagnostic::Diagnostic;
use codespan_reporting::files::SimpleFile;
use codespan_reporting::term;
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};
use lc3::vm::MemoryIndex;
use lc3::{Ast, Integer, Program, Register, ToDiagnostic, VM};
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

#[derive(Parser)]
#[clap(version)]
struct Opts {
    /// When to use color
    #[clap(short, long, arg_enum, default_value = "auto")]
    color: Color,

    #[clap(subcommand)]
    subcommand: Subcommand,
}

#[derive(ArgEnum, Debug, Clone)]
enum Color {
    #[clap(alias = "y", alias = "yes")]
    Always,
    Auto,
    #[clap(alias = "n", alias = "no")]
    Never,
}

impl Color {
    fn colorful(&self) -> bool {
        match *self {
            Self::Always => true,
            Self::Never => false,
            Self::Auto => atty::is(atty::Stream::Stdout),
        }
    }
}

#[derive(Parser)]
enum Subcommand {
    #[clap()]
    Run { input: PathBuf },
}

fn format_number(number: Integer<16>) -> String {
    format!(
        "0b{0:0>16b}  0x{0:0>4X}  {1:>+8}  {0:>8}",
        u16::from(number),
        i16::from(number)
    )
}

fn print_vm_state(vm: &VM) {
    use Register::*;

    println!(
        "     {:^18}  {:^6}  {:^8}  {:^8}",
        "binary", "hex", "2's comp", "unsigned"
    );
    let dash = String::from("-");
    println!(
        "     {}  {}  {}  {}",
        dash.repeat(18),
        dash.repeat(6),
        dash.repeat(8),
        dash.repeat(8)
    );

    for register in [R0, R1, R2, R3, R4, R5, R6, R7] {
        let value = vm.registers()[register];
        println!("{}:  {}", register.to_string(), format_number(value));
    }
}

fn print_error(err: &Diagnostic<()>, file: &Path, file_contents: &str, colorful: bool) {
    let color_choice = if colorful {
        ColorChoice::Always
    } else {
        ColorChoice::Never
    };
    let writer = StandardStream::stderr(color_choice);
    let config = term::Config::default();

    let file = SimpleFile::new(file.to_string_lossy(), file_contents);

    term::emit(&mut writer.lock(), &config, &file, err).unwrap();
}

fn run(file: &Path, colorful: bool) {
    let mut file_contents = String::new();
    File::open(file)
        .unwrap_or_else(|_| panic!("failed to open {}", file.display()))
        .read_to_string(&mut file_contents)
        .unwrap_or_else(|_| panic!("failed to read {} contents", file.display()));

    // TODO: if errors, print and exit
    let ast = match Ast::parse(&file_contents) {
        Ok(ast) => ast,
        Err(errs) => {
            for err in errs {
                print_error(&err.to_diagnostic(), file, &file_contents, colorful);
            }
            return;
        }
    };

    let program = match Program::assemble(ast) {
        Ok(program) => program,
        Err(_) => todo!(),
    };

    // if let Err(err) = program {
    //     print_error(err, file, &file_contents, colorful);
    //     return;
    // }

    let mut vm = VM::new();
    // vm.randomize_memory();

    vm.load(&program);

    vm.set_program_counter(MemoryIndex::new(0x3000));
    vm.run().unwrap();

    print_vm_state(&vm);
    println!();
    println!(
        "x30A3 = {}",
        format_number(vm.memory()[MemoryIndex::new(0x30A3)])
    );
}

fn main() {
    let opts = Opts::parse();
    let colorful = opts.color.colorful();

    let Subcommand::Run { input } = opts.subcommand;
    run(&input, colorful);
}
