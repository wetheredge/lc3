use thiserror::Error;

pub mod assembler;
pub mod ast;
mod integer;
mod register;
pub mod vm;

#[cfg(feature = "diagnostics")]
mod error_handling;
#[cfg(feature = "diagnostics")]
pub use error_handling::ToDiagnostic;

pub use assembler::Program;
pub use ast::Ast;
pub use integer::Integer;
pub use register::Register;
pub use vm::VM;

#[derive(Error, Debug, PartialEq)]
pub enum RuntimeError {
    #[error("attempted conversion from value with too many bits")]
    ConversionOverflow,

    #[error("malformed instruction")]
    MalformedInstruction,
}
