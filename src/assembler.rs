use std::collections::HashMap;

use crate::{
    ast,
    ast::{Ast, Directive, PseudoOp, Span},
    vm,
    vm::MemoryIndex,
    Integer,
};

#[derive(Debug, Clone, Copy, PartialEq)]
enum AssemblerErrorKind {
    UnexpectedOrig {
        previous: Span,
    },
    DirectiveOutsideSection {
        directive: Span,
        previous_end: Option<Span>,
    },
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct AssemblerError {
    kind: AssemblerErrorKind,
    error: Span,
}

impl AssemblerError {
    fn new(kind: AssemblerErrorKind, error: Span) -> Self {
        Self { kind, error }
    }
}

#[derive(Debug)]
pub(crate) struct MemoryBlock {
    pub start: MemoryIndex,
    pub data: Vec<Integer<16>>,
}

impl MemoryBlock {
    fn new(start: MemoryIndex) -> Self {
        Self {
            start,
            data: vec![],
        }
    }

    fn push(&mut self, value: Integer<16>) {
        self.data.push(value);
    }
}

#[derive(Debug)]
enum FirstPassItem {
    Instruction(ast::Instruction),
    String(String),
    Fill(Integer<16>),
    Block(usize),
}

#[derive(Debug)]
struct FirstPassBlock {
    start: MemoryIndex,
    data: Vec<(MemoryIndex, FirstPassItem)>,
}

impl FirstPassBlock {
    pub fn new(start: MemoryIndex) -> Self {
        Self {
            start,
            data: Vec::new(),
        }
    }

    fn push_instruction(&mut self, line: MemoryIndex, instruction: ast::Instruction) {
        self.data
            .push((line, FirstPassItem::Instruction(instruction)));
    }

    fn push_string(&mut self, line: MemoryIndex, string: String) {
        self.data.push((line, FirstPassItem::String(string)));
    }

    fn push_fill(&mut self, line: MemoryIndex, word: Integer<16>) {
        self.data.push((line, FirstPassItem::Fill(word)));
    }

    fn push_block(&mut self, line: MemoryIndex, length: usize) {
        self.data.push((line, FirstPassItem::Block(length)));
    }
}

type Labels = HashMap<String, MemoryIndex>;

#[derive(Debug)]
pub struct Program {
    pub(crate) blocks: Vec<MemoryBlock>,
    pub labels: HashMap<String, MemoryIndex>,
}

impl Program {
    pub fn assemble(ast: Ast) -> Result<Self, AssemblerError> {
        let (labels, blocks) = first_pass(ast)?;
        let blocks = second_pass(&labels, &blocks);

        Ok(Self { blocks, labels })
    }
}

fn first_pass(ast: Ast) -> Result<(Labels, Vec<FirstPassBlock>), AssemblerError> {
    let mut blocks = Vec::new();
    let mut labels = HashMap::new();

    let mut previous_end = None;
    let mut previous_orig = None;
    let mut block: Option<FirstPassBlock> = None;
    let mut current_address = MemoryIndex::new(0);
    let mut pending_labels = Vec::new();

    for line in ast {
        if let Some((_offset, label)) = line.label {
            pending_labels.push(label);
        }

        if line.directive.is_none() {
            continue;
        }

        let (offset, directive) = line.directive.unwrap();

        if block.is_none() {
            match directive {
                Directive::PseudoOp(PseudoOp::ORIG(address)) => {
                    previous_orig = Some(offset);
                    block = Some(FirstPassBlock::new(address));
                    current_address = address;
                }
                _ => {
                    return Err(AssemblerError::new(
                        AssemblerErrorKind::DirectiveOutsideSection {
                            directive: Span::single_char(0),
                            previous_end,
                        },
                        Span::from_length(offset, 1),
                    ));
                }
            }
        } else if directive == Directive::PseudoOp(PseudoOp::END) {
            blocks.push(block.take().unwrap());
            previous_end = Some(Span::from_length(offset, 1));
        } else {
            let block = block.as_mut().unwrap();

            if matches!(
                directive,
                Directive::PseudoOp(PseudoOp::FILL(_) | PseudoOp::BLKW(_) | PseudoOp::STRINGZ(_))
                    | Directive::Instruction(_)
            ) {
                for label in pending_labels.drain(..) {
                    labels.insert(label.to_string(), current_address);
                }
            }

            match directive {
                Directive::PseudoOp(PseudoOp::ORIG { .. }) => {
                    return Err(AssemblerError::new(
                        AssemblerErrorKind::UnexpectedOrig {
                            previous: Span::from_length(previous_orig.unwrap(), 1),
                        },
                        Span::from_length(offset, 1),
                    ));
                }
                Directive::PseudoOp(PseudoOp::FILL(value)) => {
                    block.push_fill(current_address, value);
                    current_address += vm::MemoryOffset::new(1);
                }
                Directive::PseudoOp(PseudoOp::BLKW(count)) => {
                    block.push_block(current_address, count);
                    current_address += vm::MemoryOffset::new(count as u16);
                }
                Directive::PseudoOp(PseudoOp::STRINGZ(string)) => {
                    let next_address = string.len() as u16 + 1;
                    block.push_string(current_address, string);
                    current_address += vm::MemoryOffset::new(next_address);
                }
                Directive::PseudoOp(PseudoOp::END) => unreachable!(),
                Directive::Instruction(instruction) => {
                    // let current_offset = block.data.len() as i16;
                    // let current_offset =
                    //     Integer::<16>::try_from(current_offset).unwrap().into();
                    // let current_address = block.start + current_offset;

                    block.push_instruction(current_address, instruction);
                    current_address += vm::MemoryOffset::new(1);
                }
            }
        }
    }

    Ok((labels, blocks))
}

fn get_offset<const BITS: u8>(
    labels: &Labels,
    offset: ast::MemoryOffset<BITS>,
    instruction_address: MemoryIndex,
) -> vm::MemoryOffset<BITS> {
    match offset {
        ast::MemoryOffset::Label(label) => {
            // TODO: deal with overflow
            let offset = labels[&label] - instruction_address;
            let offset: vm::MemoryOffset<16> = offset - vm::MemoryOffset::new(1); // PC is incremented before using the offset
            let offset: u16 = Integer::from(offset).try_into().unwrap();
            let offset: Integer<BITS> = Integer::try_from(offset).unwrap();
            vm::MemoryOffset::from(offset)
        }
        ast::MemoryOffset::Literal(offset) => offset,
    }
}

fn second_pass(labels: &Labels, blocks: &[FirstPassBlock]) -> Vec<MemoryBlock> {
    blocks
        .iter()
        .map(|FirstPassBlock { start, data }| {
            let mut memory = MemoryBlock::new(*start);

            for (address, item) in data {
                match item {
                    FirstPassItem::Instruction(instruction) => {
                        let instruction = match instruction {
                            &ast::Instruction::ADD { dest, src1, src2 } => {
                                vm::Instruction::ADD { dest, src1, src2 }
                            }
                            &ast::Instruction::AND { dest, src1, src2 } => {
                                vm::Instruction::AND { dest, src1, src2 }
                            }
                            ast::Instruction::BR { flags, to } => vm::Instruction::BR {
                                flags: *flags,
                                to: get_offset(labels, to.clone(), *address),
                            },
                            &ast::Instruction::JMP { to } => vm::Instruction::JMP { to },
                            ast::Instruction::JSR { offset } => vm::Instruction::JSR {
                                offset: get_offset(labels, offset.clone(), *address),
                            },
                            &ast::Instruction::JSRR { to } => vm::Instruction::JSRR { to },
                            ast::Instruction::LD { dest, src } => vm::Instruction::LD {
                                dest: *dest,
                                src: get_offset(labels, src.clone(), *address),
                            },
                            ast::Instruction::LDI { dest, src } => vm::Instruction::LDI {
                                dest: *dest,
                                src: get_offset(labels, src.clone(), *address),
                            },
                            ast::Instruction::LDR { dest, base, offset } => vm::Instruction::LDR {
                                dest: *dest,
                                base: *base,
                                offset: get_offset(labels, offset.clone(), *address),
                            },
                            ast::Instruction::LEA { dest, offset } => vm::Instruction::LEA {
                                dest: *dest,
                                offset: get_offset(labels, offset.clone(), *address),
                            },
                            &ast::Instruction::NOT { dest, src } => {
                                vm::Instruction::NOT { dest, src }
                            }
                            &ast::Instruction::RET => vm::Instruction::RET,
                            &ast::Instruction::RTI => vm::Instruction::RTI,
                            ast::Instruction::ST { src, dest } => vm::Instruction::ST {
                                src: *src,
                                dest: get_offset(labels, dest.clone(), *address),
                            },
                            ast::Instruction::STI { src, dest } => vm::Instruction::STI {
                                src: *src,
                                dest: get_offset(labels, dest.clone(), *address),
                            },
                            ast::Instruction::STR { src, base, offset } => vm::Instruction::STR {
                                src: *src,
                                base: *base,
                                offset: get_offset(labels, offset.clone(), *address),
                            },
                            &ast::Instruction::TRAP(vector) => vm::Instruction::TRAP(vector),
                        };

                        memory.push(instruction.into());
                    }
                    FirstPassItem::String(string) => {
                        for byte in string.bytes() {
                            memory.push(Integer::try_from(byte as u16).unwrap());
                        }
                    }
                    FirstPassItem::Fill(word) => memory.push(*word),
                    FirstPassItem::Block(count) => {
                        for _ in 0..*count {
                            memory.push(Integer::default());
                        }
                    }
                }
            }

            memory
        })
        .collect()
}
