use crate::Integer;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Register {
    R0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
}

impl From<Integer<3>> for Register {
    fn from(register: Integer<3>) -> Self {
        use Register::*;

        match u16::from(register) {
            0b000 => R0,
            0b001 => R1,
            0b010 => R2,
            0b011 => R3,
            0b100 => R4,
            0b101 => R5,
            0b110 => R6,
            0b111 => R7,
            _ => unreachable!(),
        }
    }
}

impl From<Register> for Integer<3> {
    fn from(register: Register) -> Self {
        use Register::*;

        let register: u16 = match register {
            R0 => 0b000,
            R1 => 0b001,
            R2 => 0b010,
            R3 => 0b011,
            R4 => 0b100,
            R5 => 0b101,
            R6 => 0b110,
            R7 => 0b111,
        };

        Integer::try_from(register).unwrap()
    }
}

impl ToString for Register {
    fn to_string(&self) -> String {
        use Register::*;

        match self {
            R0 => "R0".to_string(),
            R1 => "R1".to_string(),
            R2 => "R2".to_string(),
            R3 => "R3".to_string(),
            R4 => "R4".to_string(),
            R5 => "R5".to_string(),
            R6 => "R6".to_string(),
            R7 => "R7".to_string(),
        }
    }
}
