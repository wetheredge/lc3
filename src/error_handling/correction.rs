use strsim::normalized_damerau_levenshtein;

pub const PSEUDO_OPS: &[&str] = &[".blkw", ".end", ".fill", ".orig", ".stringz"];
pub const INSTRUCTIONS: &[&str] = &[
    "add", "and", "br", "brn", "brnz", "brnzp", "brp", "brz", "brzp", "jmp", "jsr", "jsrr", "ld",
    "ldi", "ldr", "not", "ret", "rti", "st", "sti", "str", "trap",
];

fn is_uppercase(s: &str) -> bool {
    let s = s.chars().filter(|c| c.is_alphabetic()).collect::<String>();

    let upper = s.chars().filter(|c| char::is_uppercase(*c)).count();

    if upper * 2 == s.len() {
        s.chars().next().map_or(false, char::is_uppercase)
    } else {
        upper > s.len() / 2
    }
}

pub fn guess_correction<'a>(options: &[&'a str], got: &str) -> Vec<String> {
    guess_correction_with_threshold(options, got, 0.6)
}

pub fn guess_correction_with_threshold<'a>(
    options: &[&'a str],
    got: &str,
    threshold: f64,
) -> Vec<String> {
    let mut options = options
        .iter()
        .filter_map(|option| {
            let distance =
                normalized_damerau_levenshtein(&option.to_uppercase(), &got.to_uppercase());

            if distance > threshold {
                Some((*option, distance))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    options.sort_by(|(_, a), (_, b)| b.partial_cmp(a).unwrap());

    let got_uppercase = is_uppercase(got);

    options
        .iter()
        .map(|(option, _)| {
            if got_uppercase {
                option.to_uppercase()
            } else {
                option.to_lowercase()
            }
        })
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;
    use test_case::test_case;

    fn guesses<'a>(instructions: &'a [&str]) -> impl Fn(Vec<String>) + 'a {
        move |guesses| assert_eq!(instructions, &guesses[..])
    }

    #[test_case("ad" => using guesses(&["add", "and"])       ; "ad to add/and")]
    #[test_case("st" => using guesses(&["st", "sti", "str"]) ; "st to st/sti/str")]
    #[test_case("et" => using guesses(&["ret"])              ; "et to ret")]
    #[test_case("nt" => using guesses(&["not"])              ; "nt to not")]
    #[test_case("ab" => Vec::<String>::new()                 ; "ab to none")]
    fn guess(input: &str) -> Vec<String> {
        guess_correction(INSTRUCTIONS, input)
    }

    #[test_case(""    => false ; "empty is lowercase")]
    #[test_case(". A" => true  ; "ignores non-letters")]
    #[test_case("Ab"  => true  ; "uppercase first letter is tie breaker")]
    #[test_case("aB"  => false ; "lowercase first letter is tie breaker")]
    fn is_uppercase(input: &str) -> bool {
        super::is_uppercase(input)
    }
}
