mod correction;

use crate::ast::{ParseError, ParseErrorKind};
use codespan_reporting::diagnostic::{Diagnostic, Label};
use correction::{guess_correction, INSTRUCTIONS, PSEUDO_OPS};

pub trait ToDiagnostic {
    fn to_diagnostic(&self) -> Diagnostic<()>;
}

fn format_list<T: std::fmt::Display>(slice: &[T]) -> String {
    match slice.len() {
        0 => "".to_string(),
        1 => slice[0].to_string(),
        2 => format!("{} or {}", slice[0], slice[1]),
        _ => {
            let last_index = slice.len() - 1;
            slice
                .iter()
                .enumerate()
                .skip(1)
                .fold(slice[0].to_string(), |s, (i, item)| {
                    s + if i == last_index { ", or " } else { ", " } + &item.to_string()
                })
        }
    }
}

impl ToDiagnostic for ParseError {
    fn to_diagnostic(&self) -> Diagnostic<()> {
        let with_message = |message| {
            Diagnostic::error()
                .with_message(message)
                .with_labels(vec![Label::primary((), self.span)])
        };
        let with_message_and_label = |message, label| {
            Diagnostic::error()
                .with_message(message)
                .with_labels(vec![Label::primary((), self.span).with_message(label)])
        };

        let diagnostic = match &self.kind {
            ParseErrorKind::ExpectedToken { found, expected } => {
                let expected = format_list(expected);
                let label = format!("expected {expected}, found {found}");
                with_message_and_label("unexpected token", label)
            }
            ParseErrorKind::ExtraTokens => with_message("extra token(s) after last operand"),
            ParseErrorKind::InvalidBranchFlags => with_message("invalid branch flags"),
            ParseErrorKind::InvalidDigit { base } => {
                with_message(&format!("invalid digit in {base} literal"))
            }
            ParseErrorKind::InvalidInstruction(got) => {
                let diagnostic = with_message("invalid instruction");
                let suggestions = guess_correction(INSTRUCTIONS, got);
                if suggestions.is_empty() {
                    diagnostic
                } else {
                    diagnostic
                        .with_notes(vec![format!("did you mean {}?", format_list(&suggestions))])
                }
            }
            ParseErrorKind::InvalidOperandKind { found, expected } => with_message_and_label(
                "invalid signature",
                format!("expected {expected}, found {found}"),
            ),
            ParseErrorKind::InvalidPseudoOp(got) => {
                let diagnostic = with_message("invalid pseudo op");
                let suggestions = guess_correction(PSEUDO_OPS, got);
                if suggestions.is_empty() {
                    diagnostic
                } else {
                    diagnostic
                        .with_notes(vec![format!("did you mean {}?", format_list(&suggestions))])
                }
            }
            ParseErrorKind::LiteralOutOfRange(error) => {
                with_message_and_label("out of range number", format!("{error}"))
            }
            ParseErrorKind::MissingOperandSeparator => {
                with_message("missing comma between operands")
            }
            ParseErrorKind::TooFewOperands { found, expected } => with_message_and_label(
                "too few operands",
                format!("expected {expected} operands, found {found}"),
            ),
            ParseErrorKind::UnclosedString => with_message("unclosed string"),
            ParseErrorKind::UnexpectedCharacter => with_message("unexpected character"),
        };

        diagnostic
    }
}
