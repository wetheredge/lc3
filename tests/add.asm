	.ORIG x3000

	; Load data
	LD R0, A
	LD R1, B

	; R0 = R0 + R1
	ADD R0, R0, R1

	; Store data
	ST R0, Result

	HALT
	.END

	.ORIG x300A
A:	.FILL #3
B:	.FILL #-2
Result:	.BLKW #1
	.END
