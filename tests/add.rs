use pretty_assertions::assert_eq;

const PROGRAM: &str = include_str!("./add.asm");

#[test]
fn add() {
    let program = lc3::Program::assemble(lc3::Ast::parse(PROGRAM).unwrap()).unwrap();
    let mut vm = lc3::VM::new();

    vm.load(&program);
    vm.set_program_counter(0x3000.into());
    vm.run().unwrap();

    let result_address = program.labels["Result"];
    assert_eq!(1, i16::from(vm.memory()[result_address]));
}
